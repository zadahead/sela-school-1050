import './Btn.css';

export const Btn = (props) => {
    return (
        <button onClick={props.onClick} className='Btn'>
            {props.children}
        </button>
    )
}