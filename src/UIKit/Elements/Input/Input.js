import './Input.css';

export const Input = (props) => {

    const handleOnChange = (e) => {
        const value = e.target.value;
        props.onChange(value)
    }

    return (
        <div className='Input'>
            <input {...props} onChange={handleOnChange} />
        </div>
    )
}