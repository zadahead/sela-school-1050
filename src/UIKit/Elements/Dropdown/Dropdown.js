import { useEffect, useState } from 'react';
import { Between } from '../../Layouts/Line/Line';
import './Dropdown.css';
export const Dropdown = (props) => {
    const [isOpen, setIsOpen] = useState(true);

    useEffect(() => {
        document.body.addEventListener('click', handleClose);

        return () => {
            document.body.removeEventListener('click', handleClose)
        }
    }, [])

    const handleToggle = (e) => {
        e.stopPropagation();

        setIsOpen(!isOpen);
    }

    const handleSelect = (i) => {
        console.log('handleSelect', i)
        props.onChange(i.id);
        setIsOpen(false);
    }

    const handleClose = () => {
        console.log('handleClose');
        setIsOpen(false);
    }

    const renderList = () => {
        if(isOpen) {
            return (
                <div className='list'>
                    {
                        props.list.map(i => (
                            <div key={i.id} onClick={() => handleSelect(i)}>{i.name}</div>
                        ))
                    }
                </div>
            )
        }
    }

    const renderTitle = () => {
        const selectedId = props.selected;
        const list = props.list;

        const selectedItem = list.find(i => i.id === selectedId);

        if(selectedItem) {
            return selectedItem.name;
        }
        return 'Please Select';
    }
    return (
        <div className="Dropdown">
            <div className='trig' onClick={handleToggle}>
                <Between>
                    <div>{renderTitle()}</div>
                    <i className="fas fa-chevron-down"></i>
                </Between>
            </div>
            {renderList()}
        </div>
    )
} 