import './App.css';
import { Line, Between } from "./UIKit/Layouts/Line/Line";
import { Grid } from './UIKit/Layouts/Grid/Grid';

import { Route, Routes, Link, NavLink } from 'react-router-dom';
import { Login } from './Views/Login';
import { About } from './Views/About';

import { DropDownView } from './Views/DropDownView';
import { HooksView } from './Views/Hooks';
import { ContextView } from './Views/Context';

import { CounterProvider } from './Context/counterContext';
import { CounterAdd } from './Components/CounterWrap/CounterAdd';

export const App = () => {

    return (
        <CounterProvider>
            <div className="App">
                <Grid>
                    <div>
                        <Between>
                            <Line>
                                <i className="fas fa-user"></i>
                                <h4>React 1050</h4>
                                <CounterAdd />
                            </Line>
                            <Line>
                                <NavLink to='/login'>login</NavLink>
                                <NavLink to='/about'>about</NavLink>
                                <NavLink to='/dropdown'>dropdown</NavLink>
                                <NavLink to='/hooks'>hooks</NavLink>
                                <NavLink to='/context'>context</NavLink>
                            </Line>
                        </Between>
                    </div>
                    <div>
                        <Routes>
                            <Route path='/dropdown' element={<DropDownView />} />
                            <Route path='/login' element={<Login />} />
                            <Route path='/about' element={<About />} />
                            <Route path='/hooks' element={<HooksView />} />
                            <Route path='/context' element={<ContextView />} />
                        </Routes>
                    </div>
                </Grid>
            </div>
        </CounterProvider>
    )
}

export const SpecialApp = () => {
    return (
        <div>
            <h3>Sepcial App</h3>
            <App />
        </div>
    )
}
