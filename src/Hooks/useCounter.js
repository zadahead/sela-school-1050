import { useState } from "react";

export const useCounter = (initialValue) => {
    const [count, setCount] = useState(initialValue || 0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return [
        count,
        handleAdd
    ]
}