import { useState } from "react";

export const useColorSwitcher = () => {
    const [color, setColor] = useState('red');

    const handleSwitch = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }


    const styleCss = {
        color: color,
        backgroundColor: 'yellow'
    }

    return [
        color,
        handleSwitch,
        styleCss
    ]
}