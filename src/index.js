import React from 'react';
import ReactDOM from 'react-dom/client';

import { BrowserRouter as Router } from 'react-router-dom';

import * as All from './App';

/*
move to the folder you want to install in

git clone https://gitlab.com/zadahead/sela-school-1050.git

cd sela-school-1050

if you want to run it: 

npm install
npm start

if you want to open it: 
code .
*/


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
    <All.App />
  </Router>
);


