/*
    <User />     => nane, age, id  ==> #1: Mosh (25)
    <Users />    => <User .... /> , <User .... />
*/

class WelcomeClass extends React.Component {
  render = () => {
    return (
      <div>
        <h2>Welcome Something111</h2>
      </div>
    )
  }
}

const Welcome = () => {
  return (
    <div>
      <h2>Welcome Something</h2>
    </div>
  )
}

//Class
class UserClass extends React.Component {
  render = (props) => {
    console.log('props', props);
    return (
      <div>
        <h2>#{this.props.id}: {this.props.name} ({this.props.age})</h2>
      </div>
    )
  }
}

//Function
const User = (props) => {
  return (
    <div>
      <h2>#{props.id}: {props.name} ({props.age})</h2>
    </div>
  )
}

const Users = () => {

  return (
    <div>
      <User id="1" name="Mosh" age="35" />
      <User id="2" name="David" age="45" />
      <User id="3" name="Siso" age="55" />
    </div>
  )
}


const Alerter = (props) => {
  const alertThis = () => {
    alert(props.msg);
  }

  return (
    <button onClick={alertThis}>Alert</button>
  )
}

const Handler = (props) => {
  return (
    <button onClick={props.handleClick}>{props.btn}</button>
  )
}

const App = (props) => {
  const handleClick = () => {
    console.log('blabla')
  }

  const handleAnother = () => {
    console.log('booommm')
  }

  return (
    <div>
      <h1>Hello {props.name}</h1>
      <Handler handleClick={handleClick} btn="bla" />
      <Handler handleClick={handleAnother} btn="boom" />
    </div>
  )
}




const Btn = (props) => {
  return (
    <button onClick={props.handleClick}>{props.title}</button>
  )
}

const Welcome1 = (props) => {
  const handleClick = () => {
    console.log('Clicked!!');
  }

  return (
    <div>
      <h1>Hello {props.name}</h1>
      <Btn title="Bla" handleClick={handleClick} />
    </div>
  )
}

/*
  initialState
  state
  setState
  
  render
*/
class Counter extends React.Component {
  state = {
    count: 1,
    isRed: true
  }

  handleAdd = () => {
    this.setState({
      count: this.state.count + 1
    })
  }

  render = () => {
    console.log('render');

    const styleCss = {
      color: 'red'
    }

    return (
      <div>
        <h1 style={styleCss}>Count, {this.state.count}</h1>
        <button onClick={this.handleAdd}>Add +1</button>
      </div>
    )
  }
}


class ColorSwitcher extends React.Component {
  state = {
    isRed: true
  }

  handleSwitch = () => {
    this.setState({
      isRed: !this.state.isRed
    })
  }

  render = () => {

    const styleCss = {
      color: this.state.isRed ? 'red' : 'blue'
    }

    return (
      <div>
        <h1 style={styleCss}>Color Switcher, {styleCss.color}</h1>
        <button onClick={this.handleSwitch}>Switch</button>
      </div>
    )
  }
}









const CounterWrap = () => {
  const [count, setCount] = useState(0);

  const handleAdd = () => {
    setCount(count + 1);
  }

  return (
    <>
      <Counter count={count} handleAdd={handleAdd} />
      <Counter count={count * 3} handleAdd={handleAdd} />
      <Counter count={count + 4} handleAdd={handleAdd} />
    </>
  )
}

const Counter = (props) => {
  console.log('render', props);

  return (
    <div>
      <h2>Count, {props.count}</h2>
      <button onClick={props.handleAdd}>Add</button>
    </div>
  )
}

