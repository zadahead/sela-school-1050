import { useState } from "react"
import { Dropdown } from "../UIKit/Elements/Dropdown/Dropdown"

const list = [
    { id: 1, name: 'mosh' },
    { id: 2, name: 'david' },
    { id: 3, name: 'sss' },
    { id: 4, name: 'caro' }
]

export const DropDownView = () => {
    const [selected, setSelected] = useState(null);
    return (
        <div>
            <h1>Drop Down</h1>
            <Dropdown list={list} selected={selected} onChange={setSelected} />
        </div>
    )
}