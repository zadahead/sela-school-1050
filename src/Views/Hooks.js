
import Users from "../Components/Users"

export const HooksView = () => {
    return (
        <div>
            <h2>Custom Hooks</h2>
            <div>
                <Users />
            </div>
        </div>
    )
}