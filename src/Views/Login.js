import { useState } from "react";
import  { Line } from '../UIKit/Layouts/Line/Line';
import { Input } from "../UIKit/Elements/Input/Input";
import { Btn } from "../UIKit/Elements/Btn/Btn";

export const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        console.log(username);
        console.log(password);
    }

    return (
        <div>
            <h1>Login Page</h1>
            <div>
                <Line>
                    <Input
                        placeholder='username'
                        value={username}
                        onChange={setUsername}
                    />
                    <Input
                        placeholder='password'
                        value={password}
                        onChange={setPassword}
                    />
                    <Btn onClick={handleLogin}>Login</Btn>
                </Line>
            </div>
        </div>
    )
}