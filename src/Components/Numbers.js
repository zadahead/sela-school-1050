import { useEffect } from "react";
import axios from "axios";

const list = [
    1, 2, 3, 4, 5, 5, 7
]


const Numbers = () => {

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(reps => {
                console.log(reps);
            })
    }, [])

    const renderList = () => {
        return list.map((num, index) => {
            return <h4 key={index}>{num}</h4>
        })
    }

    return (
        <div>
            <h3>Numbers List:</h3>
            {renderList()}
        </div>
    )
}

export default Numbers;
