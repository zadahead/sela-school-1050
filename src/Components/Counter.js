import { useCounter } from "../Hooks/useCounter";



const Counter = () => {
    //logic
    const [count, handleAdd] = useCounter();

    //render
    return (
        <div>
            <h2>Count, {count}</h2>
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}

export default Counter;