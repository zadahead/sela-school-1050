import { useState } from "react";
import CounterLC from "./FuncLifeCycle";

const Toggler = () => {
    const [isDisplay, setIsDisplay] = useState(true);

    const handleToggle = () => {
        setIsDisplay(!isDisplay);
    }

    const renderComponent = () => {
        if(isDisplay) {
            return <CounterLC />;
        }
        return null;
    }


    return (
        <div>
            <button onClick={handleToggle}>Toggle</button>
            {renderComponent()}
        </div>
    )

}

export default Toggler;