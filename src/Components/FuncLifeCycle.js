/*
    componentDidMount
    componentDidUpdate
    componentWillUnmount
*/

import { useState, useEffect } from "react";

const CounterLC = () => {
    console.log('render');

    const [count, setCount] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        console.log('componentDidMount');

        document.body.addEventListener('click', logThis);

        setTimeout(() => {
            setIsLoading(false);
        }, 50);

        return () => {
            console.log('componentWillUnmount');
            document.body.removeEventListener('click', logThis);
        }

    }, [])

    useEffect(() => {
        console.log('componentDidUpdate', isLoading)

        return () => {
            console.log('special unmount', isLoading);
        }
    }, [isLoading]);


    const logThis = () => {
        console.log('body click')
    }

    const handleAdd = () => {
        setCount(count + 1);
    }

    if(isLoading) {
        return <h4>Loading...</h4>
    }
    
    return (
        <div>
            <h2>Count, {count}</h2>
            <button onClick={handleAdd}>Add +1</button>
        </div>
    )
}

export default CounterLC;