import { useColorSwitcher } from "../Hooks/useColorSwitcher";


export const ColorSwitcher = () => {
    //logic
    const [color, handleSwitch, styleCss] = useColorSwitcher();

    //render
    return (
        <div>
            <h2 style={styleCss}>Color Switcher, {color}</h2>
            <button onClick={handleSwitch}>Add</button>
        </div>
    )
}
