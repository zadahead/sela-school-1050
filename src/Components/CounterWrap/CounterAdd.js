import { useContext } from "react"
import { counterContext } from "../../Context/counterContext"

export const CounterAdd = () => {
    const { handleAdd } = useContext(counterContext);

    return (
        <button onClick={handleAdd}>Add + 1</button>
    )
}