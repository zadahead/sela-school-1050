import { useContext } from "react"
import { counterContext } from "../../Context/counterContext"

export const CounterDisplay = () => {
    const { count } = useContext(counterContext);

    return (
        <div>
            <h2>Count, {count}</h2>
        </div>
    )
}