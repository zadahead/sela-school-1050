import { CounterAdd } from "./CounterAdd";
import { CounterDisplay } from "./CounterDisplay";

export const Wrapper = () => {

    return (
        <div>
            <CounterDisplay />
            <CounterAdd />
        </div>
    )
}