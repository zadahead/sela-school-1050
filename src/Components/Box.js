const Box = (props) => {
    console.log(props);
    return (
        <div style={styleCss.wrapper}>
            <h2>{props.title}</h2>
            <h4>{props.info}</h4>
            <div style={styleCss.content}>
                {props.children}
            </div>
        </div>
    )
}

const styleCss = {
    wrapper: {
        border: '1px solid #e1e1e1',
        padding: '10px',
        margin: '10px'
    },

    content: {
        backgroundColor: '#e1e1e1',
        padding: '10px'
    }
}

export default Box;