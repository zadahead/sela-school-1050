import React from 'react';
/*
    componentDidMount
    componentDidUpdate
    componentWillUnmount
*/



class CounterLC extends React.Component {
    state = {
       count: 1,
    }

    componentDidMount = () => {
        console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }
  
    handleAdd = () => {
        this.setState({
          count: this.state.count + 1
       })
    }
  
    render = () => {
      console.log('render');

      return (
        <div>
          <h1>Count, {this.state.count}</h1>
          <button onClick={this.handleAdd}>Add +1</button>
        </div>
      )
    }
  }

  export default CounterLC;
  