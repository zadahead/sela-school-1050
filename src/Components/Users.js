import { useEffect, useState } from "react";
import axios from "axios";

/*
1) take current state
2) render based on current state

-) update state
*/


// useFetch(url)

const useFetch = (url) => {
    const [list, setList] = useState(null);
    const [error, setError] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        console.log('will run onload only');
        setError(false);
        setIsLoading(true);

        axios.get(`https://jsonplaceholder.typicode.com${url}`)
            .then(resp => {
                console.log(resp);
                setList(resp.data);
                setIsLoading(false);
            })
            .catch(e => {
                setError(e.message);
                setIsLoading(false);
            })

    }, [url]);

    return {
        list,
        error,
        isLoading
    };
}

// Fetch

const Fetch = (props) => {
    const { list, error, isLoading } = useFetch(props.url);

    //render
    if (error) {
        return <h2 style={{ color: 'red' }}>{error}</h2>
    }

    if (isLoading) {
        return <h2>loading...</h2>
    }



    return (
        <div>
            {props.onRender(list)}
        </div>
    )
}

const Users = () => {

    const onRender = (list) => {
        return list.map(user => {
            return <h4 key={user.id}>{user.title}</h4>
        })
    }

    return (
        <Fetch
            url='/todos'
            onRender={onRender}
        />
    )
}

export default Users;