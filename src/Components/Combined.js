import { useCounter } from '../Hooks/useCounter';
import { useColorSwitcher } from '../Hooks/useColorSwitcher';

export const Combined = () => {
    const [count, setCount] = useCounter(5);
    const [color, handleSwitch, styleCss] = useColorSwitcher();

    return (
        <div>
            <h2 style={styleCss}>Combined, {count} - {color}</h2>
            <div>
                <button onClick={setCount}>Add +1</button>
                <button onClick={handleSwitch}>Change Color</button>
            </div>
        </div>
    )
}