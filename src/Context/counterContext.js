import { createContext, useState } from "react";

export const counterContext = createContext({});

export const CounterProvider = (props) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1)
    }

    return (
        <counterContext.Provider value={{ count, handleAdd }}>
            {props.children}
        </counterContext.Provider>
    )
}